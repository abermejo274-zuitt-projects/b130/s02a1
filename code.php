<?php

$products = [];

function createProduct($name, $price){
    global $products;
    array_push($products, [
        'name' => $name,
        'price' => $price
    ]);
}

function printProducts($products){
    foreach($products as $product){
        echo "</br>";
        foreach($product as $name => $price){
            echo "<li>$name : $price</li>";
        }
    }
}

function countProducts($products) {
    echo "Total: ".count($products);
}

function deleteProduct(){
    global $products;
	array_pop($products);
}

?>