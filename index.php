<?php require "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array Manipulation</title>
</head>
<body>
    <h1>Activity</h1>

    <?php
        createProduct("apple", 20);
        createProduct("mango", 25);
        createProduct("strawberry", 30);
        createProduct("banana", 15);
    ?>

    <pre>
        <ul>
            <?php
                printProducts($products);
            ?>
        </ul>
    </pre>

    <p>
        <?php
            countProducts($products);
        ?>
    </p>

    <pre>
		<?php 
            deleteProduct();
            print_r($products);
		 ?>
	</pre>
    
</body>
</html>